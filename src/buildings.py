from utilities import world_to_grid, window_to_world, zeros
from constants import TILE_SIZE, TILE_NUMBER, Type, Image, Owner, Actions
from units import recompute_A_star, order_building, Unit


total_resources = 0


class Tile:
    def __init__(self, x, y, image, scale, batch):  # World coord
        self.x = x
        self.y = y
        self.image = image
        self._scale = scale
        self.default_scale = scale
        self.type = Type.TILE
        self.width = image.width * scale
        self.height = image.height * scale
        self.batch = batch
        self.wall = False
        self.draw_outline = False


    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        ratio = value / self._scale
        self.width *= ratio
        self.height *= ratio
        self._scale = value


class BuildingRegistry:
    def __init__(self):
        self.walls = []
        self.array = zeros((TILE_NUMBER + 1, TILE_NUMBER + 1))


    def add_building(self, building, units, grid):
        i, j = world_to_grid(building.x, building.y)
        i = int(i)
        j = int(j)
        if self.array[i][j] != 0:
            print(f"Tile {(i, j)} occupied")
        else:
            self.array[i][j] = building
            height, width = building.final_height // TILE_SIZE, building.final_width // TILE_SIZE
            height = int(height)
            width = int(width)
            for w in range(i, i + width):
                for h in range(j, j + height):
                    self.walls.append((w, h))
        recompute_A_star(units, grid)

    def delete_building(self, building, units, grid):
        i, j = world_to_grid(building.x, building.y)
        i = int(i)
        j = int(j)
        print(i, j)
        if self.array[i][j] == 0:
            print(f"no building at tile {(i, j)}")
        else:
            height = int(building.final_height // TILE_SIZE)
            width = int(building.final_width // TILE_SIZE)
            to_remove = []
            for iteration, wall in enumerate(self.walls):
                if wall == (i, j):
                    for w in range(i, i + width):
                        for h in range(j, j + height):
                            to_remove.append(iteration)
            for iteration in range(len(to_remove) - 1, -1, -1):
                self.walls.pop(to_remove[iteration])
            self.array[i][j] = 0
            recompute_A_star(units, grid)

    def add_tile(self, tile):
        i, j = world_to_grid(tile.x, tile.y)
        i = int(i)
        j = int(j)
        self.array[i][j] = tile

    def delete_tile(self, tile):
        i, j = world_to_grid(tile.x, tile.y)
        i = int(i)
        j = int(j)
        self.array[i][j] = 0

    def get_list(self):
        output_list = []
        for list in self.array:
            for item in list:
                if item != 0:
                    output_list.append(item)
        return output_list


def get_walls(buildings):
    return buildings.walls


def get_building_array(buildings):
    return buildings.array


def get_building_list(buildings):
    return buildings.get_list()


def add_building(buildings, building, units, grid):
    return buildings.add_building(building, units, grid)


def remove_building(buildings, building, units, grid):
    return buildings.delete_building(building, units, grid)


def add_tile(tiles, tile):
    return tiles.add_tile(tile)


def remove_tile(tiles, tile):
    return tiles.delete_building(tile)


def update_building(building):
    # building.changed_sprite = False
    if building.finished is False:
        building.changed_sprite = True
        building.update_build_animation()
        if building.cooldown == 0:
            finish_building(building)
    else:
        if len(building.queue) > 0:
            item = building.queue[0]
            print(item[1])
            if item[1] == 0:
                building.add_to_world.append((building.x + building.width,
                                              building.y))
                building.queue.pop(0)
            else:
                item[1] -= 1



def update_buildings(buildings, camera, unit_batch):
    units_to_add = []
    for bld in get_building_list(buildings):
        global total_resources
        update_building(bld)
        if bld.resources > 0:
            total_resources += bld.resources
            bld.resources = 0
            print(f"total resources : {total_resources}")
        while len(bld.add_to_world) > 0:
            for unit_coordinates in bld.add_to_world:
                units_to_add.append(Unit(*unit_coordinates,
                                         Image.unit,
                                         1 / 2,
                                         unit_batch))
                bld.add_to_world.pop(0)
    return units_to_add


class Building(Tile):
    def __init__(self, x, y, image, scale, batch, finished=True, owner=Owner.PLAYER, is_base=False, *arg, **kwargs):  # World coord
        super().__init__(x, y, image, scale, batch, *arg, **kwargs)
        self.wall = True
        self.type = Type.BUILDING
        self.name = "building"
        self.finished = finished
        self.changed_sprite = True
        self.total_cooldown = 120
        self.final_x = x
        self.final_y = y
        self.final_width = image.width * scale
        self.final_height = image.height * scale
        self.collectible = False
        self.is_base = is_base
        self.resources = 0
        self.owner = owner
        self.selected = False

        self.buttons = []
        if self.is_base:
            self.buttons.append(Actions.PROD_UNIT)

        self.queue = []
        self.add_to_world = []

        if self.finished is False:
            image = Image.unit_building
            self.cooldown = self.total_cooldown
            self.update_build_animation()
        else:
            image = Image.wall
            self.cooldown = 0


    def update_build_animation(self):
        self.cooldown = max(0, self.cooldown - 1)
        multiplier = (self.total_cooldown - self.cooldown) / self.total_cooldown
        self.x = self.final_x + self.final_width / 4
        self.y = self.final_y + self.final_height / 4
        self.x -= self.final_width / 4 * (0.5 + multiplier / 2)
        self.y -= self.final_height / 4 * (0.5 + multiplier / 2)
        self.scale = self.default_scale * multiplier


def finish_building(building):
    building.image = Image.wall
    building.finished = True
    building.changed_sprite = True


def start_build_mode(image, scale, build_gui):
    build_gui.scale = scale
    build_gui.default_scale = scale
    build_gui.size = (image.width * scale, image.height * scale)
    build_gui.active = True


def finish_build_mode(unit, camera, batch, graph, cursor, build_gui, shift=False):
    x, y = window_to_world(*cursor.position, camera)
    x = int(x / TILE_SIZE) * TILE_SIZE + TILE_SIZE // 2
    y = int(y / TILE_SIZE) * TILE_SIZE + TILE_SIZE // 2
    building = Building(x, y,
                        Image.unit_building, build_gui.scale,
                        batch, finished=False)
    unit.to_build.append(building)
    build_gui.active = False
    order_building(unit, graph, camera, cursor, shift)


def queue_production(building, type):
    cooldown = 60
    building.queue.append([type, cooldown])
