# Controls

* Select : Left mouse button
* Move : Right mouse button
* Queue order : SHIFT + Right mouse button
* Pan camera : Arrows
* Zoom in/out : +/-
* Select all units : CTRL + A
* Add wall : P
* Remove wall : O
* Add unit : M
* Remove unit : L


# TODO List

- [X] Add background registry class
- [ ] Improve general input method
- [X] Implement resources
- [X] Implement resource collection
- [X] Implement resource carriage to base
- [X] Building action
- [X] External map loading
- [ ] Actions prerequisites (resource based and more)
- [X] Select buildings
- [ ] Display unit/buildings life
- [ ] Improve GUI
- [ ] Improve minimap (continuous move, colours, show buildings, selected units)
- [ ] Improve unit production
- [ ] Different size buildings_changed


# Issues
- [X] Building stops building when out of sight
- [X] Fix resource collection interruption
- [ ] Performance issues
