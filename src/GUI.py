from utilities import world_to_window, draw_box, window_to_world, window_to_grid, grid_to_window, draw_full_box
from constants import WINDOW_SIZE, BAR_SIZE, WORLD_SIZE, GRID_NUMBER, Image, TILE_SIZE, Actions, Type
from buildings import start_build_mode, queue_production
from pyglet import shapes
from pyglet.text import Label


class BottomBar:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.width = WINDOW_SIZE
        self.height = BAR_SIZE

    def update(self, batch):
        # shapes.Rectangle(self.x, self.y, self.width, self.height, color=(20, 20, 64), batch=batch)
        pass

    def draw(self, batch):
        shapes.Rectangle(self.x, self.y, self.width, self.height, color=(200, 200, 140), batch=batch)
        # draw_box(self.x, self.y, self.width, self.height, batch)
        # for i in range(65):
        #     draw_box(self.x + i, self.y + i, self.width - 2 * i, self.height - 2 * i)


class Minimap:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.width = BAR_SIZE
        self.height = BAR_SIZE

    def get_world_coordinates(self, x, y):
        x_out = (x - self.x) / self.width * WORLD_SIZE
        y_out = (y - self.y) / self.height * WORLD_SIZE
        return x_out, y_out

    def draw(self, units, buildings, camera, batch):
        draw_box(self.x, self.y, self.width, self.height, batch)
        i = (camera.offset_x / WORLD_SIZE) * self.width
        j = (camera.offset_y / WORLD_SIZE) * self.height
        pixel_x = camera.width / WORLD_SIZE * self.width
        pixel_y = camera.height / WORLD_SIZE * self.height
        draw_box(int(self.x + i), int(self.y + j), pixel_x, pixel_y, batch)
        for unit in units:
            i = (unit.x / WORLD_SIZE) * self.width
            j = (unit.y / WORLD_SIZE) * self.height
            pixel_size = self.height / GRID_NUMBER
            draw_full_box(int(self.x + i), int(self.y + j), pixel_size, pixel_size, batch)
        for building in buildings:
            i = (building.final_x / WORLD_SIZE) * self.width
            j = (building.final_y / WORLD_SIZE) * self.height
            pixel_size = self.height / GRID_NUMBER
            draw_full_box(int(self.x + i), int(self.y + j), pixel_size, pixel_size, batch)


class Button:
    def __init__(self, x, y, width, height, action, origin):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.action = action
        self.origin = origin

    def handle_left_click(self, unit, build_gui):
        print(f"button {self.x, self.y} clicked")
        if self.action == Actions.BUILD:
            start_build_mode(Image.wall, 1 / 4, build_gui)
        if self.action == Actions.PROD_UNIT:
            queue_production(self.origin, Type.DUMMY)

    def draw(self, batch):
        draw_box(self.x, self.y, self.width, self.height, batch)


class Buttons:
    def __init__(self):
        self.list = []

    def build_buttons(self, actions, origin):
        self.list = []
        btn_size = 50
        btn_margin = 9
        n = len(actions)
        for i in range(WINDOW_SIZE - btn_size - btn_margin, BAR_SIZE, -btn_size - btn_margin):
            for j in range(BAR_SIZE - btn_size - btn_margin, btn_margin, -btn_size - btn_margin):
                if n == 0:
                    return None
                action = actions[len(actions) - n]
                self.list.append(Button(i, j, btn_size, btn_size, action, origin))
                n -= 1

    def reset_buttons(self):
        self.list = []

    def draw(self, batch):
        for button in self.list:
            button.draw(batch)


class Build_GUI:
    def __init__(self):
        self.active = False
        self.default_scale = 1
        self.scale = 1
        self.size = (0, 0)


    def draw(self, cursor, camera, batch):
        if self.active:
            x, y = cursor.position
            # x -= self.size[0] // 2
            # y -= self.size[1] // 2

            x = int(x / TILE_SIZE)
            y = int(y / TILE_SIZE)
            x *= TILE_SIZE
            y *= TILE_SIZE

            delta_x = ((camera.offset_x / TILE_SIZE) % 1) * TILE_SIZE
            delta_y = ((camera.offset_y / TILE_SIZE) % 1) * TILE_SIZE

            x -= delta_x
            y -= delta_y

            # x = max(0, x)
            # y = max(0, y)
            # x = min(WINDOW_SIZE - self.size[0], x)
            # y = min(WINDOW_SIZE - self.size[1], y)

            draw_box(x, y + BAR_SIZE, *self.size, batch)


class StaticGUI:
    def __init__(self):
        self.list = []


def draw_selected(object, camera, batch):
    if object.selected:
        window_x, window_y = world_to_window(object.x, object.y, camera)
        window_y += BAR_SIZE
        draw_box(int(window_x - object.width // 2),
                 int(window_y - object.height // 2),
                 object.width,
                 object.height,
                 batch=batch)
