from utilities import mid, window_to_world, draw_box
from constants import WINDOW_SIZE, BAR_SIZE, WORLD_SIZE, Owner


class Cursor:
    position = 0, 0


class Selector:
    def __init__(self, x, y):
        self.selection = []
        self.initial_x = x
        self.initial_y = y
        self.final_x = x
        self.final_y = y
        self.active = False

    def update(self, units, buildings, x, y, camera, dt):  # x, y = window coordinates
        if self.active:
            self.final_x = x
            self.final_y = y + BAR_SIZE
            x_min = min(self.final_x, self.initial_x)
            x_max = max(self.final_x, self.initial_x)
            y_min = min(self.final_y, self.initial_y)
            y_max = max(self.final_y, self.initial_y)

            y_min -= BAR_SIZE
            y_max -= BAR_SIZE
            x_min, y_min = window_to_world(x_min, y_min, camera)
            x_max, y_max = window_to_world(x_max, y_max, camera)

            selected_units = []

            for unit in units:
                if x_min < unit.x < x_max and y_min < unit.y < y_max:
                    if unit.owner == Owner.PLAYER:
                        selected_units.append(unit)
                        unit.selected = True
                else:
                    unit.selected = False


            selected_buildings = []
            for building in buildings:
                if x_min < building.final_x < x_max and y_min < building.final_y < y_max:
                    if building.owner == Owner.PLAYER:
                        selected_buildings.append(building)
                        building.selected = True
                else:
                    building.selected = False
            if len(selected_units) > 0:
                self.selection = selected_units
                for building in selected_buildings:
                    building.selected = False
            else:
                self.selection = selected_buildings

    def start_selection(self, x, y, buttons):  # x, y = window coordinates
        self.active = True
        self.initial_x = x
        self.initial_y = y
        self.final_x = x
        self.final_y = y
        # for unit in self.selection:
        #     unit.selected = False
        self.selection = []
        buttons.reset_buttons()


    def end_selection(self, x, y, buttons):  # x, y = window coordinates
        print(f"selected ({len(self.selection)}) : ", end="")
        for selected in self.selection:
            print(selected.name, end=" ")
        print(" ")
        self.active = False
        self.final_x = x
        self.final_y = y
        if len(self.selection) == 1:
            buttons.build_buttons(self.selection[0].buttons, self.selection[0])


    def draw(self, batch):
        # global WINDOW_SIZE
        if self.active:
            x_min = min(self.final_x, self.initial_x)
            y_min = min(self.final_y, self.initial_y)

            start_x = mid(self.initial_x, 1, WINDOW_SIZE)
            start_y = mid(self.initial_y, 1 + BAR_SIZE, WINDOW_SIZE + BAR_SIZE)
            end_x = mid(self.final_x, 1, WINDOW_SIZE)
            end_y = mid(self.final_y, 1 + BAR_SIZE, WINDOW_SIZE + BAR_SIZE)
            x_min = min(start_x, end_x)
            y_min = min(start_y, end_y)

            draw_box(x_min, y_min, abs(end_x - start_x), abs(end_y - start_y), batch)



class Camera:
    def __init__(self):
        self.default_size = 512
        self.width = self.default_size
        self.height = self.default_size
        self.offset_x = 0
        self.offset_y = 0
        self.zoom = 1
        self.pan_right = False
        self.pan_left = False
        self.pan_up = False
        self.pan_down = False
        self.zoom_out = False
        self.zoom_in = False
        self.just_moved = True


    @property
    def offset_x(self):
        return self._offset_x

    @offset_x.setter
    def offset_x(self, new_offset_x):
        self._offset_x = mid(0, new_offset_x, WORLD_SIZE - self.width)

    @property
    def offset_y(self):
        return self._offset_y

    @offset_y.setter
    def offset_y(self, new_offset_y):
        self._offset_y = mid(0, new_offset_y, WORLD_SIZE - self.height)

    @property
    def zoom(self):
        return self._zoom

    @zoom.setter
    def zoom(self, new_zoom):
        self._zoom = round(new_zoom, 2)
        width = self.width
        height = self.height
        self.width = self.default_size // new_zoom
        self.height = self.default_size // new_zoom
        delta_x = width - self.width
        delta_y = height - self.height
        self.offset_x += delta_x / 2
        self.offset_y += delta_y / 2

    def update(self, all_objects, build_gui):

        self.just_moved = False
        if self.pan_down:
            self.offset_y = max(0, self.offset_y - 5 - self.zoom)
            self.just_moved = True
        if self.pan_up:
            self.offset_y = min(WORLD_SIZE - self.height, self.offset_y + 5 + self.zoom)
            self.just_moved = True
        if self.pan_left:
            self.offset_x = max(0, self.offset_x - 5 - self.zoom)
            self.just_moved = True
        if self.pan_right:
            self.offset_x = min(WORLD_SIZE - self.width, self.offset_x + 5 + self.zoom)
            self.just_moved = True

        if self.zoom_in:
            self.zoom = min(self.zoom + 0.02, 2)
            self.just_moved = True
            self.update_zoom(all_objects, build_gui)

        if self.zoom_out:
            self.zoom = max(self.zoom - 0.02, 1)
            self.just_moved = True
            self.update_zoom(all_objects, build_gui)

    def update_zoom(self, objects, build_gui):
        for obj in objects:
            if obj != 0:
                obj.scale = obj.default_scale * self.zoom
        build_gui.scale = build_gui.default_scale * self.zoom
