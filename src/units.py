from utilities import grid_to_world, world_to_grid, grid_to_window, world_to_window, draw_line, window_to_grid
from constants import FPS, BAR_SIZE, Type, Status, Owner, Actions
from pathfinding import A_star, A_star_wall, find_closest_base


def update_unit(units, unit, graph, buildings, dt):
    # print(unit.status)
    if unit.dead:
        remove_unit(units, unit)
    if len(unit.path) > 0:
        move_unit(unit, dt)
    elif len(unit.queue) > 0:
        iterate_queue(unit, graph)
    elif unit.status == Status.COLLECTING:
        update_collection(unit)
    elif unit.status == Status.BACK_AT_BASE:
        i, j = unit.closest_base
        buildings[i][j].resources += unit.resources_collected
        unit.resources_collected = 0
        iterate_collection(unit)
    else:
        unit.status = Status.IDLE


def iterate_queue(unit, graph):
    next_in_queue = unit.queue.pop(0)
    if next_in_queue[0] == Status.MOVE:
        set_target(unit, *next_in_queue[1], graph)
    elif next_in_queue[0] == Status.BUILDING:
        unit_build(unit)
    elif next_in_queue[0] == Status.COLLECTING:
        # unit.status = Status.COLLECTING
        start_collecting(unit)
    elif next_in_queue[0] == Status.BACK_AT_BASE:
        start_back_at_base(unit)
        # unit.status = Status.BACK_AT_BASE
        # order_back_to_base(unit)


def order_collection(unit, graph, resource_coord, buildings, shift):
    # print('order collection')
    unit.collects_resource = resource_coord
    i, j = find_closest_base(buildings, resource_coord, graph)
    unit.closest_base = i, j
    unit.is_collecting = True
    if not shift:
        # add_target(unit, *coordinates, graph)
        reset_queue(unit)
    add_to_queue(unit, Status.MOVE, resource_coord)
    add_to_queue(unit, Status.COLLECTING, resource_coord)


def iterate_collection(unit):
    add_to_queue(unit, Status.MOVE, unit.collects_resource)
    add_to_queue(unit, Status.COLLECTING, unit.collects_resource)


def order_back_to_base(unit):
    # unit.status = Status.BACK_AT_BASE
    if unit.closest_base is not None:
        reset_queue(unit)
        add_to_queue(unit, Status.MOVE, unit.closest_base)
        add_to_queue(unit, Status.BACK_AT_BASE, unit.collects_resource)
    else:
        reset_queue(unit)  # if no closest base : unit idle


def update_collection(unit):
    unit.resources_collected += 1
    if unit.resources_collected % 60 == 0:
        # print(f"{unit.resources_collected} resources -> back to base")
        order_back_to_base(unit)



def start_collecting(unit):
    unit.status = Status.COLLECTING


def start_back_at_base(unit):
    unit.status = Status.BACK_AT_BASE


def recompute_A_star(units, grid):
    for unit in units:
        if unit.status != Status.IDLE:
            if len(unit.path) > 0:
                set_target(unit, *unit.path[-1], grid)


def move_unit(unit, dt):
    unit.target_x, unit.target_y = grid_to_world(unit.path[0][0], unit.path[0][1])
    x, y, tx, ty = unit.x, unit.y, unit.target_x, unit.target_y
    dist = ((unit.x - unit.target_x)**2 + (unit.y - unit.target_y)**2) ** 0.5
    if dist > 1:
        dx = tx - x
        dy = ty - y
        unit.x += (dx * unit.speed / dist) * FPS * dt
        unit.y += (dy * unit.speed / dist) * FPS * dt
    else:
        unit.path.pop(0)


def set_target(unit, x, y, graph):  # Takes grid coordinates
    if unit.status != Status.BUILDING:  # Can't move if building
        x_grid, y_grid = world_to_grid(unit.x, unit.y)
        if (x, y) in graph.walls:
            unit.path, _ = A_star_wall(graph, (x_grid, y_grid), (x, y))
            unit.path.pop(0)
        else:
            unit.path, _ = A_star(graph, (x_grid, y_grid), (x, y))
            unit.path.pop(0)
            unit.status = Status.MOVE


def add_target(unit, x, y, graph):  # Takes grid coordinates
    if unit.status != Status.BUILDING:
        if len(unit.path) == 0:
            set_target(unit, x, y, graph)
        else:
            # start_x, start_y = unit.path[-1]
            # x_grid, y_grid = world_to_grid(unit.x, unit.y)
            add_to_queue(unit, Status.MOVE, (x, y))
            # unit.status = Status.MOVE


def draw_unit_path(unit, camera):
    if unit.status == Status.MOVE:
        if len(unit.path) > 0:
            x1, y1 = grid_to_window(*unit.path[0], camera)
            y1 += BAR_SIZE
            x2, y2 = world_to_window(unit.x, unit.y, camera)
            y2 += BAR_SIZE
            draw_line(x1, y1, x2, y2)
        for i in range(len(unit.path) - 1):
            x1, y1 = grid_to_window(*unit.path[i + 1], camera)
            y1 += BAR_SIZE
            x2, y2 = grid_to_window(*unit.path[i], camera)
            y2 += BAR_SIZE
            draw_line(x1, y1, x2, y2)


def order_building(unit, graph, camera, cursor, shift):
    i, j = window_to_grid(*cursor.position, camera)
    if not shift:
        reset_queue(unit)
    add_to_queue(unit, Status.MOVE, (i, j))
    add_to_queue(unit, Status.BUILDING, (i, j))


def order_move(unit, coordinates, shift):
    if not shift:
        reset_queue(unit)
    add_to_queue(unit, Status.MOVE, coordinates)


def add_to_queue(unit, type, coordinates):
    unit.queue.append((type, coordinates))


def reset_queue(unit):
    unit.queue = []
    unit.path = []


def unit_build(unit):
    unit.dead = True
    unit.finished_building.append(unit.to_build[0])


class UnitRegistry:
    def __init__(self, units=[]):
        self.units = units

    def add_unit(self, unit):
        self.units.append(unit)

    def remove_unit(self, unit):
        if unit in self.units:
            self.units.remove(unit)


class Unit:
    def __init__(self, x, y, image, scale, batch, owner=Owner.PLAYER):
        self.status = Status.IDLE
        self.speed = 2
        self.selected = False
        self.path = []
        self.queue = []  # Format = [('action1', (x1, y1)), ('action2', (x2, y2))]
        self.x = x
        self.y = y
        self.dead = False
        self.type = Type.DUMMY
        self.name = "dummy"
        self.image = image
        self._scale = scale
        self.default_scale = scale
        self.width = image.width * scale
        self.height = image.height * scale
        self.batch = batch
        self.to_build = []
        self.finished_building = []
        self.resources_collected = 0
        self.closest_base = None
        self.collects_resource = None
        self.is_collecting = True
        self.owner = owner
        self.buttons = [Actions.BUILD]

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        ratio = value / self._scale
        self.width *= ratio
        self.height *= ratio
        self._scale = value


def add_unit(units, unit):
    units.add_unit(unit)


def remove_unit(units, unit):
    units.remove_unit(unit)


def get_units(units):
    return units.units


def create_unit(type, x, y, batch):
    pass
