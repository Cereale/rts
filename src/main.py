import pyglet
from pyglet.window import key, mouse, FPSDisplay
from pyglet.sprite import Sprite
from random import randint
from utilities import window_to_grid, world_to_window, window_to_world, on_camera, in_square, remover, load_map, grid_to_world
from buildings import Building, BuildingRegistry, Tile, update_buildings, get_building_list, get_building_array, add_building, remove_building, get_walls, finish_build_mode, add_tile
from constants import WINDOW_SIZE, TILE_SIZE, TILE_NUMBER, BAR_SIZE, WORLD_SIZE, GRID_NUMBER, GRID_SIZE, FPS, Image, Type, Owner, MapElements
from interface import Camera, Cursor, Selector
from pathfinding import SquareGrid, get_neighbourhood, get_direct_neighbourhood
from GUI import BottomBar, Minimap, Button, draw_selected, Build_GUI, StaticGUI, Buttons
from pyglet import shapes
from units import Unit, update_unit, UnitRegistry, add_unit, remove_unit, get_units, order_collection, order_move  # draw_unit_path


class Sprites:
    def __init__(self):
        self.list = []


def init_game(width, height):
    def build_sprites(objects, camera, is_background=False):
        visible = on_camera(objects, camera, is_background)
        sprites_out = []
        for i, obj in enumerate(visible):

            x, y = world_to_window(obj.x, obj.y, camera)
            sprites_out.append(Sprite(x=x,
                                      y=y + BAR_SIZE,
                                      img=obj.image,
                                      batch=obj.batch))
            sprites_out[i].scale = obj.scale
            sprites_out[i].type = obj.type
        return sprites_out



    unit_batch = pyglet.graphics.Batch()
    background_batch = pyglet.graphics.Batch()
    building_batch = pyglet.graphics.Batch()
    GUI_batch = pyglet.graphics.Batch()
    static_GUI_batch = pyglet.graphics.Batch()

    pyglet.resource.path = ['../assets']
    pyglet.resource.reindex()
    map = load_map('../bitmaps/test_map.bmp', (TILE_NUMBER, TILE_NUMBER))

    background = BuildingRegistry()
    for i in range(TILE_NUMBER + 1):
        for j in range(TILE_NUMBER + 1):
            image = Image.grass
            tile = Tile(TILE_SIZE * (i + 0.5),
                        TILE_SIZE * (j + 0.5),
                        image,
                        1 / 4,
                        background_batch)
            add_tile(background, tile)

    window = pyglet.window.Window(width, height + BAR_SIZE)
    fps_display = FPSDisplay(window)
    # keyboard = key.KeyStateHandler()
    # window.push_handlers(keyboard)

    cursor = Cursor()
    camera = Camera()
    buildings = BuildingRegistry()
    build_gui = Build_GUI()
    static_gui = StaticGUI()
    static_gui.list.append(shapes.Rectangle(0, 0, WINDOW_SIZE, BAR_SIZE,
                                            color=(200, 200, 140),
                                            batch=static_GUI_batch))
    grid = SquareGrid(GRID_NUMBER, GRID_NUMBER, get_walls(buildings))

    units = UnitRegistry()
    for i in range(TILE_NUMBER):
        for j in range(TILE_NUMBER):
            pixel = map[i][j]
            x, y = grid_to_world(i, j)
            if pixel == MapElements.unit:
                unit = Unit(x,
                            y,
                            Image.unit,
                            1 / 2,
                            unit_batch)
                add_unit(units, unit)
            if pixel == MapElements.resource:
                resource = Building(x,
                                    y,
                                    Image.resources,
                                    1 / 4,
                                    building_batch,
                                    owner=Owner.WORLD)
                resource.collectible = True
                resource.name = 'resource'
                add_building(buildings, resource, get_units(units), grid)
            if pixel == MapElements.base:
                base = Building(x,
                                y,
                                Image.base,
                                1 / 4,
                                building_batch,
                                is_base=True)
                add_building(buildings, base, get_units(units), grid)
            if pixel == MapElements.wall:
                base = Building(x,
                                y,
                                Image.wall,
                                1 / 4,
                                building_batch)
                add_building(buildings, base, get_units(units), grid)



    selector = Selector(*cursor.position)
    all_objects = []
    all_objects += get_units(units)
    all_objects += get_building_list(buildings)
    all_objects += get_building_list(background)
    sprites = Sprites()
    sprites.list = build_sprites(get_units(units), camera)
    sprites.list += build_sprites(get_building_array(background), camera, True)
    sprites.list += build_sprites(get_building_array(buildings), camera, True)

    # GUI
    bottom_bar = BottomBar()
    minimap = Minimap()
    buttons = Buttons()


    def update_all_sprites():
        nonlocal GUI_batch
        GUI_batch = pyglet.graphics.Batch()  # Empty GUI_batch
        sprites.list = build_sprites(get_units(units), camera)
        sprites.list += build_sprites(get_building_array(background), camera, True)
        sprites.list += build_sprites(get_building_array(buildings), camera, True)

    def update(dt):
        nonlocal sprites

        all_objects = []
        all_objects += get_units(units)
        all_objects += get_building_list(buildings)
        all_objects += get_building_list(background)
        grid.walls = get_walls(buildings)
        camera.update(all_objects, build_gui)
        bottom_bar.update(GUI_batch)
        selector.update(get_units(units), get_building_list(buildings), *cursor.position, camera, dt)
        buildings_changed = False

        units_to_add = update_buildings(buildings, camera, unit_batch)

        for unit in units_to_add:
            add_unit(units, unit)


        for bld in get_building_list(buildings):
            if bld != 0 and bld.changed_sprite:
                buildings_changed = True

        for unit in get_units(units):
            update_unit(units, unit, grid, get_building_array(buildings), dt)
            for bld in unit.finished_building:
                remover(selector.selection)
                add_building(buildings, bld, get_units(units), grid)
                buildings_changed = True
                camera.update_zoom(get_building_list(buildings), build_gui)
            unit.finished_building = []
        if buildings_changed:
            update_all_sprites()
        if camera.just_moved:
            update_all_sprites()
        else:
            previous_sprites = []
            for sprite in sprites.list:
                if sprite.type != Type.DUMMY:
                    previous_sprites.append(sprite)
            new_sprites = build_sprites(get_units(units), camera)
            sprites.list = previous_sprites + new_sprites


        buttons.draw(GUI_batch)


    @window.event
    def on_draw():
        nonlocal GUI_batch

        window.clear()
        # Drawing batches
        background_batch.draw()
        building_batch.draw()
        unit_batch.draw()

        build_gui.draw(cursor, camera, GUI_batch)
        selector.draw(GUI_batch)
        for unit in get_units(units):
            # unit.draw_path(camera)
            draw_selected(unit, camera, GUI_batch)
        for building in get_building_list(buildings):
            draw_selected(building, camera, GUI_batch)
        static_GUI_batch.draw()
        fps_display.draw()
        minimap.draw(get_units(units), get_building_list(buildings), camera, GUI_batch)
        bottom_bar.draw(GUI_batch)

        GUI_batch.draw()

    @window.event
    def on_mouse_press(x, y, button, modifiers):
        on_minimap = ((minimap.x < x < minimap.x + minimap.width) and
                      (minimap.y < y < minimap.y + minimap.height))
        shift = False
        if modifiers & key.MOD_SHIFT:
            shift = True
        if button == mouse.LEFT:
            if on_minimap:
                x, y = minimap.get_world_coordinates(x, y)
                camera.offset_x, camera.offset_y = x - camera.width / 2, y - camera.height / 2
                update_all_sprites()
            elif y < BAR_SIZE:
                if len(selector.selection) == 1:
                    for button in buttons.list:
                        if in_square(x, y,
                                     button.x, button.y,
                                     button.width, button.height):
                            button.handle_left_click(selector.selection[0], build_gui)
                            break
            else:
                if len(selector.selection) == 1 and build_gui.active:
                    unit = selector.selection[0]

                    finish_build_mode(unit, camera,
                                      building_batch,
                                      grid,
                                      cursor,
                                      build_gui,
                                      shift)
                else:
                    selector.start_selection(x, y, buttons)
        if button == mouse.RIGHT and len(selector.selection) > 0:
            if len(selector.selection) == 1 and build_gui.active:
                build_gui.active = False
            elif on_minimap or y > BAR_SIZE:
                if on_minimap:
                    target_x, target_y = world_to_window(*minimap.get_world_coordinates(x, y), camera)
                if y > BAR_SIZE:
                    target_x, target_y = cursor.position[0], cursor.position[1]

                i, j = window_to_grid(target_x,
                                      target_y,
                                      camera)
                if (i, j) in get_walls(buildings) and get_building_array(buildings)[i][j].collectible:
                    for unit in selector.selection:
                        order_collection(unit, grid, (i, j), get_building_list(buildings), shift)

                else:
                    neighbourhood = get_neighbourhood(grid,
                                                      (i, j),
                                                      len(selector.selection))

                    for k, unit in enumerate(selector.selection):
                        order_move(unit, neighbourhood[k], shift)

    @window.event
    def on_key_press(button, modifiers):
        if button == key.S:
            for unit in selector.selection:
                unit.path = []
                unit.queue = []

        if button == key.DOWN:
            camera.pan_down = True
        if button == key.UP:
            camera.pan_up = True
        if button == key.LEFT:
            camera.pan_left = True
        if button == key.RIGHT:
            camera.pan_right = True
        if button in [key.PLUS, key.NUM_ADD]:
            camera.zoom_in = True
        if button in [key.MINUS, key.NUM_SUBTRACT]:
            camera.zoom_out = True

        if button == key.P:
            x, y = window_to_world(*cursor.position, camera)
            x = x // GRID_SIZE * GRID_SIZE + GRID_SIZE / 2
            y = y // GRID_SIZE * GRID_SIZE + GRID_SIZE / 2
            building = Building(x, y, Image.wall, 1 / 4, building_batch)
            add_building(buildings, building, get_units(units), grid)
            camera.update_zoom(get_building_list(buildings), build_gui)
            update_all_sprites()

        if button == key.O:
            i, j = window_to_grid(*cursor.position, camera)
            building = get_building_array(buildings)[i][j]
            if building != 0:
                remove_building(buildings, building, get_units(units), grid)
                camera.update_zoom(get_building_list(buildings), build_gui)
                update_all_sprites()

        if button == key.L:
            x, y = window_to_world(*cursor.position, camera)
            for unit in get_units(units):
                u_x, u_y, u_w, u_h = unit.x, unit.y, unit.width, unit.height
                u_x -= u_w / 2
                u_y -= u_h / 2
                if in_square(x, y, u_x, u_y, u_w, u_h):
                    remove_unit(units, unit)

        if button == key.M:
            x, y = window_to_world(*cursor.position, camera)
            add_unit(units, Unit(x,
                                 y,
                                 Image.unit,
                                 1 / 2,
                                 unit_batch))


        if button == key.A and key.MOD_CTRL:
            selector.selection = []
            for unit in get_units(units):
                selector.selection.append(unit)
                unit.selected = True

    @window.event
    def on_key_release(button, modifiers):

        if button == key.DOWN:
            camera.pan_down = False
        if button == key.UP:
            camera.pan_up = False
        if button == key.LEFT:
            camera.pan_left = False
        if button == key.RIGHT:
            camera.pan_right = False
        if button in [key.PLUS, key.NUM_ADD]:
            camera.zoom_in = False
        if button in [key.MINUS, key.NUM_SUBTRACT]:
            camera.zoom_out = False

    @window.event
    def on_mouse_release(x, y, button, modifiers):
        selector.end_selection(x, y, buttons)


    @window.event
    def on_mouse_motion(x, y, dx, dy):
        cursor.position = [x, y - BAR_SIZE]
        # on_minimap = ((minimap.x < x < minimap.x + minimap.width) and
        #               (minimap.y < y < minimap.y + minimap.height))

    @window.event
    def on_mouse_drag(x, y, dx, dy, button, modifiers):
        cursor.position = [x, y - BAR_SIZE]

    # Run application
    pyglet.clock.schedule_interval(update, 1 / FPS)
    pyglet.app.run()


if __name__ == '__main__':
    init_game(WINDOW_SIZE, WINDOW_SIZE)
