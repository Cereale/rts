from collections import deque
import heapq
from copy import deepcopy
from utilities import world_to_grid
# from time import sleep


class SimpleGraph:
    def __init__(self):
        self.edges = {}

    def neighbours(self, id):
        return self.edges[id]


class Queue:
    def __init__(self):
        self.elements = deque()

    def empty(self):
        return len(self.elements) == 0

    def put(self, x):
        self.elements.append(x)

    def get(self):
        return self.elements.popleft()


class PriorityQueue:
    def __init__(self):
        self.elements = []

    def empty(self):
        return len(self.elements) == 0

    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))

    def get(self):
        return heapq.heappop(self.elements)[1]


def get_neighbourhood(graph, start, n):
    frontier = Queue()
    frontier.put(start)
    reached = {}
    reached[start] = True
    tiles = [start]

    while not frontier.empty() and len(tiles) < n:
        current = frontier.get()
        for neighbour in graph.neighbours(current):
            if len(tiles) >= n:
                break
            if neighbour not in reached:
                frontier.put(neighbour)
                reached[neighbour] = True
                tiles.append((neighbour))
    return tiles


def get_direct_neighbourhood(graph, start):
    return graph.neighbours(start)


class SquareGrid:
    def __init__(self, width, height, walls):
        self.w = width
        self.h = height
        self.walls = set(walls)

    def in_bounds(self, id):
        (x, y) = id
        return ((0 <= x < self.w) and (0 <= y < self.h))

    def passable(self, id):
        return id not in self.walls

    def neighbours(self, id):
        (x, y) = id
        results = [(x + 1, y), (x, y - 1),
                   (x - 1, y), (x, y + 1),
                   (x + 1, y + 1), (x - 1, y - 1),
                   (x - 1, y + 1), (x + 1, y - 1)]
        results = filter(self.in_bounds, results)
        results = filter(self.passable, results)
        return list(results)

    def draw(self):
        for i in range(self.w):
            for j in range(self.h):
                if (i, j) in self.walls:
                    print(" # ", end='')
                else:
                    print(" . ", end='')
            print("")


def heuristic(a, b):
    # Euclidian
    return ((a[0] - b[0])**2 + (a[1] - b[1])**2)**0.5


def reconstruct_path(came_from, current):
    total_path = [current]
    while current in came_from:
        current = came_from[current]
        total_path.append(current)
    return total_path


def A_star(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    came_from = {}
    cost_so_far = {}
    came_from[start] = None
    cost_so_far[start] = 0
    # iterations = 0
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            path = reconstruct_path(came_from, current)
            path.reverse()
            path.pop(0)
            return path, cost_so_far[goal]

        for neighbour in graph.neighbours(current):
            if neighbour not in cost_so_far:
                cost_so_far[neighbour] = 10**10
            tentative_cost = cost_so_far[current] + heuristic(current, neighbour)
            if tentative_cost < cost_so_far[neighbour]:
                came_from[neighbour] = current
                cost_so_far[neighbour] = tentative_cost
                priority = cost_so_far[neighbour] + heuristic(goal, neighbour)
                frontier.put(neighbour, priority)
    return [start], cost_so_far[start]


def A_star_wall(graph, start, goal):
    graph_copy = deepcopy(graph)
    graph_copy.walls.remove(goal)
    path, cost = A_star(graph_copy, start, goal)
    path.pop(-1)
    return path, cost


def get_closest(graph, start, goals):
    frontier = Queue()
    frontier.put(start)
    reached = {}
    reached[start] = True
    tiles = [start]

    graph_copy = deepcopy(graph)
    for goal in goals:
        graph_copy.walls.remove(goal)

    neigh_list = []
    while not frontier.empty():
        current = frontier.get()
        for neighbour in graph_copy.neighbours(current):
            neigh_list.append(neighbour)
            if neighbour not in reached:
                frontier.put(neighbour)
                reached[neighbour] = True
                tiles.append((neighbour))
                if neighbour in goals:
                    # print("in_loop")
                    return neighbour
    # print(neigh_list)
    # print(goals)
    return None


def get_closest_2(graph, start, goals):
    graph_copy = deepcopy(graph)
    min_goal = None
    min_cost = 1.e100
    for goal in goals:
        graph_copy.walls.remove(goal)
        path, cost = A_star(graph_copy, start, goal)
        if cost < min_cost:
            # print("inside, loop", cost)
            min_cost = cost
            min_goal = goal
    print(min_goal, min_cost)
    return min_goal


def find_closest_base(building_list, coordinates, graph):
    goals = []
    for bld in building_list:
        if bld.is_base:
            x, y = world_to_grid(bld.final_x, bld.final_y)
            goals.append((int(x), int(y)))
    output = get_closest(graph, coordinates, goals)
    print(output)
    return output
