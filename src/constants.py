import pyglet
from enum import IntEnum

WINDOW_SIZE = 512
WORLD_SIZE = 2048
TILE_SIZE = 32
GRID_SIZE = 32
GRID_NUMBER = WORLD_SIZE // GRID_SIZE
TILE_NUMBER = WORLD_SIZE // TILE_SIZE
BAR_SIZE = 128
FPS = 60


pyglet.resource.path = ['../assets']
pyglet.resource.reindex()


class Type(IntEnum):
    DUMMY = 0
    TILE = 1
    BUILDING = 2


class Status(IntEnum):
    IDLE = 0
    MOVE = 1
    BUILDING = 2
    COLLECTING = 3
    BACK_AT_BASE = 4


class Owner(IntEnum):
    PLAYER = 0
    WORLD = 1


class Actions(IntEnum):
    BUILD = 0
    PROD_UNIT = 1


class Image:
    def get_image(name, centered=True):
        image = pyglet.resource.image(name)
        if centered:
            image.anchor_x = image.width // 2
            image.anchor_y = image.height // 2
        return image

    unit = get_image("unit.png")
    projectile = get_image("projectile.png")
    grass = get_image("grass.png")
    wall = get_image("wall.png")
    unit_building = get_image("unit_building.png")
    resources = get_image("resources.png")
    base = get_image("base.png")
    none = get_image("none.png")
    pixel = get_image("p_white.bmp")


class MapElements:
    void = [0, 0, 0]
    wall = [255, 0, 0]
    resource = [91, 110, 225]
    unit = [255, 243, 0]
    base = [255, 255, 255]

    list = [void, wall, resource, unit, base]
