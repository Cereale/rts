from pyglet.gl import glBegin, glVertex3f, glEnd, GL_LINES
import pyglet
from constants import TILE_SIZE, WINDOW_SIZE, WORLD_SIZE, GRID_SIZE, MapElements
import numpy as np
import PIL.Image as PIL_Image
# from pyglet.shapes import Line


def distance(p1=(0, 0), p2=(0, 0)):
    return ((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)**(1 / 2)


def in_square(x, y, x_sq, y_sq, w, h):
    x_condition = (x_sq < x < x_sq + w)
    y_condition = (y_sq < y < y_sq + h)
    return (x_condition and y_condition)


def mid(x, y, z):
    list = [x, y, z]
    list.sort()
    return list[1]


def world_to_grid(x, y, camera=0):
    i = x // GRID_SIZE
    j = y // GRID_SIZE
    return (int(i), int(j))


def grid_to_world(i, j, camera=0):
    x = (i + 0.5) * GRID_SIZE
    y = (j + 0.5) * GRID_SIZE
    return (x, y)


def window_to_world(x, y, camera):
    x_world = (x * camera.width / WINDOW_SIZE) + camera.offset_x
    y_world = (y * camera.height / WINDOW_SIZE) + camera.offset_y
    return (x_world, y_world)


def world_to_window(x, y, camera):
    x_window = (x - camera.offset_x) * WINDOW_SIZE / camera.width
    y_window = (y - camera.offset_y) * WINDOW_SIZE / camera.height
    return (x_window, y_window)


def window_to_grid(x, y, camera):
    world_x, world_y = window_to_world(x, y, camera)
    out_x, out_y = world_to_grid(world_x, world_y, camera)
    return int(out_x), int(out_y)


def grid_to_window(x, y, camera):
    world_x, world_y = grid_to_world(x, y, camera)
    x_out, y_out = world_to_window(world_x, world_y, camera)
    return x_out, y_out


def on_camera(objects, camera, id_on_grid=False):
    selected_objects = []
    if id_on_grid is True:
        i_min = int(camera.offset_x // TILE_SIZE)
        i_max = min(int(2 + (camera.offset_x + camera.width) // TILE_SIZE),
                    WORLD_SIZE // TILE_SIZE + 1)
        j_min = int(camera.offset_y // TILE_SIZE)
        j_max = min(int(2 + (camera.offset_y + camera.height) // TILE_SIZE),
                    WORLD_SIZE // TILE_SIZE + 1)
        for i in range(i_min, i_max):
            for j in range(j_min, j_max):
                if objects[i][j] != 0:
                    selected_objects.append(objects[i][j])
    else:
        for obj in objects:
            cond1 = (obj.x < camera.offset_x + camera.width + obj.width)
            cond2 = (obj.x + obj.width > camera.offset_x)
            cond3 = (obj.y < camera.offset_y + camera.height + obj.height)
            cond4 = (obj.y + obj.height > camera.offset_y)
            if cond1 & cond2 & cond3 & cond4:
                selected_objects.append(obj)
    return(selected_objects)


def remover(set):
    to_remove = []
    for i, unit in enumerate(set):
        if unit.dead:
            to_remove.append(i)
    for i in range(len(to_remove) - 1, -1, -1):
        set.pop(to_remove[i])


def zeros(size):
    array = []
    for i in range(size[0]):
        array.append([])
        for j in range(size[1]):
            array[i].append(0)
    return array


def load_map(filename, size):
    image = np.asarray(PIL_Image.open(filename))
    array = zeros(size)
    n = 0
    for i in range(size[0]):
        for j in range(size[1]):
            for element in MapElements.list:
                if (image[i][j] == element).all():
                    array[i][j] = element
                    n += 1
    if n != size[0] * size[1]:
        print("not all pixels are loaded.\nSome color values might not be registered")
    return array


def draw_box(x, y, w, h, batch, color=(255, 255, 255)):
    # glBegin(GL_QUADS)
    # glVertex3f(x + 1, y, -1)
    # glVertex3f(x + 1, y + h, -1)
    # glVertex3f(x + w, y + 1, -1)
    # glVertex3f(x + w, y + h, -1)
    # glEnd()
    x = int(x)
    y = int(y)
    w = int(w)
    h = int(h)

    group = pyglet.graphics.Group()
    batch.add_indexed(4, pyglet.gl.GL_LINES,
                      group,
                      [0, 1, 0, 2, 2, 3, 1, 3],
                      ('v2i', (x, y,
                               x, y + h,
                               x + w, y,
                               x + w, y + h))
                      )


def draw_full_box(x, y, w, h, batch):
    x = int(x)
    y = int(y)
    w = int(w)
    h = int(h)
    group = pyglet.graphics.Group()
    batch.add_indexed(4, pyglet.gl.GL_QUAD_STRIP,
                      group,
                      [0, 1, 2, 3],
                      ('v2i', (x, y,
                               x, y + h,
                               x + w, y,
                               x + w, y + h))
                      )


def draw_line(x1, y1, x2, y2, batch):
    pyglet.graphics.draw_indexed(2, pyglet.gl.GL_LINES,
                                 [0, 1],
                                 ('v2i', (x1, y1,
                                          x2, y2))
                                 )
    # glBegin(GL_LINES)
    # glVertex3f(x1, y1, -1)
    # glVertex3f(x2, y2, -1)
    # glEnd()
